## aws con terraform desplegar cluster eks

Aprenderemos a crear:
- una vpc
- internet Gatewey
- crear subredes públicas y privadas
- Nat Gateway
- Private route table privadas y públicas
- el cluster de kubernetes
- balanceador de carga
- pods


## Al terminar deberían tener una arquitectura como se visualiza en la imagen a continuación

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-eks.png?alt=media&token=08ca570b-0e94-42df-9c62-5383e474d085
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-eks.png?alt=media&token=08ca570b-0e94-42df-9c62-5383e474d085
)

## ¿Qué es EKS?

Amazon Elastic Kubernetes Service (Amazon EKS) es un servicio es un servicio almacenado en la nube que te permite ejecutar Kubernetes sin necesidad de crear ni mantener su propio plano de control de Kubernetes. Kubernetes es un sistema de código abierto utilizado para administración y automatización de las aplicaciones en contenedores.

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/AWS-EKS-logo-no-background.png?alt=media&token=88642c49-1c3d-4cb7-905f-5b75e70656b9
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/AWS-EKS-logo-no-background.png?alt=media&token=88642c49-1c3d-4cb7-905f-5b75e70656b9
)


## ¿Qué es Terraform?

Software utilizado para crear infraestructura como código. Permite a los usuarios definir y configurar la infraestructura en un lenguaje, generando un plan de ejecución para aprovisionar los recursos de infraestructura en proveedores de nube tales como AWS, IBM Cloud.

Para realizar el lanzamiento del clusters es necesario realizar la descarga y la instalación de Terraform como se muestra a continuación.

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/terraform.png?alt=media&token=343317b7-4aae-499e-ba14-2479bc711010
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/terraform.png?alt=media&token=343317b7-4aae-499e-ba14-2479bc711010
)


## pre requisitos

- Tener instalado Terraform
https://www.terraform.io/downloads.html
- Se recomienda tener un sistema operativo linux
- Tener una cuenta creada en aws
- Tener instalado kubectl
- Tener instalado eksctl


## Vamos a crear un usuario en aws

Primero es necesario ir a la consola de aws

https://console.aws.amazon.com/

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/consola%20aws.png?alt=media&token=71448b01-92a0-47c7-8302-e432b7aa94ce
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/consola%20aws.png?alt=media&token=71448b01-92a0-47c7-8302-e432b7aa94ce
)

Debemos buscar la opción IAM
- AWS Identity and Access Management (IAM) permite controlar de forma segura el acceso a los servicios y recursos de AWS.

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/IAM.png?alt=media&token=b0c9d8ae-71ef-4313-b9c2-72f16ea14b8c
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/IAM.png?alt=media&token=b0c9d8ae-71ef-4313-b9c2-72f16ea14b8c
)

Dar clic en users

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/users.png?alt=media&token=a64bf42a-2673-4992-b157-51a0c2fba798
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/users.png?alt=media&token=a64bf42a-2673-4992-b157-51a0c2fba798
)

Dar clic en Add users

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/addusers.png?alt=media&token=9c101e39-b046-4eae-95fc-52802ede1e34
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/addusers.png?alt=media&token=9c101e39-b046-4eae-95fc-52802ede1e34
)

Ingresar el "User name" en la caja de texto

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/username.png?alt=media&token=712a6da1-f900-4cce-b948-46c0d31dbd4b
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/username.png?alt=media&token=712a6da1-f900-4cce-b948-46c0d31dbd4b
)

Seleccionar la opción acceso programático

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/programmatic%20acceso.png?alt=media&token=a267c47b-c880-4622-9358-0fe206e97b84
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/programmatic%20acceso.png?alt=media&token=a267c47b-c880-4622-9358-0fe206e97b84
)

Dar clic en Permisos siguientes

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/next%20permissions.png?alt=media&token=69f6ad8a-1b7e-40cd-b150-463de4d936c3
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/next%20permissions.png?alt=media&token=69f6ad8a-1b7e-40cd-b150-463de4d936c3
)

Dar clic en "adjuntar políticas existentes directamente"
Seleccionar la Política "AdministratorAccess"
Dar clic en "siguientes etiquetas" 

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/attach%20policy.png?alt=media&token=80b680cb-bb27-450a-92fa-cd1eb891f845
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/attach%20policy.png?alt=media&token=80b680cb-bb27-450a-92fa-cd1eb891f845
)

Dar clic en "próxima revisión"

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/next%20review.png?alt=media&token=e39e2bfa-b4f9-47d7-bb84-d91db0f56c5f
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/next%20review.png?alt=media&token=e39e2bfa-b4f9-47d7-bb84-d91db0f56c5f
)

Dar clic en "crear usuario"

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/crear%20usuario.png?alt=media&token=8bd053bd-1347-4715-b3ee-993bf48bfcd1
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/crear%20usuario.png?alt=media&token=8bd053bd-1347-4715-b3ee-993bf48bfcd1
)

Se debe guardar el archivo con "Download.csv"
Se debe guardar el "Access key ID"
Se debe guardar el "Secret access key"

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/claves.png?alt=media&token=c6d18c74-5109-4bb8-9da3-965be1f9635e
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/claves.png?alt=media&token=c6d18c74-5109-4bb8-9da3-965be1f9635e
)

Iniciamos con la configuración del perfil de terraform desde este punto los siguientes comandos se 
ejecutan desde una terminal

```sh
    aws configure --profile prueba

```

la terminal nos pide el Access key y debemos ingresar el Access key que guardamos anteriormente al crear el usuario

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/access%20key.png?alt=media&token=49492ffb-a07d-40d3-a5b6-a65120c2d001
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/access%20key.png?alt=media&token=49492ffb-a07d-40d3-a5b6-a65120c2d001
)

la terminal nos pide el Secret Access key y debemos ingresar el Secret Access key que guardamos anteriormente al crear el usuario

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/secret%20access%20key.png?alt=media&token=f557c8a2-fdd5-4b78-a956-5eac0c2ffaa2
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/secret%20access%20key.png?alt=media&token=f557c8a2-fdd5-4b78-a956-5eac0c2ffaa2
)

para las opciones:
- Default region name [None]: 
- Default output format [None]:

Se puede dar enter para dejar la opción por defecto

podemos validar si el perfil quedo creado con exito con el siguiente comando

```bash
 cat ~/.aws/config
```

A continuación creamos un archivo con nombre "provider.tf" e ingresamos la siguiente configuración

```json
    provider "aws" {
        # definimos el perfil
        profile = "prueba"
        # definimos la región
        region = "us-east-1"
    }

```

A continuación creamos un archivo con nombre "vpc.tf" e ingresamos la siguiente configuración

```json
resource "aws_vpc" "main" {
  cidr_block                       = "192.168.0.0/16" # El bloque CIDR para la VPC
  instance_tenancy                 = "default" # Una opción de arrendamiento para instancias lanzadas
  enable_dns_support               = true # para habilitar / deshabilitar el soporte de DNS en el
  enable_dns_hostnames             = true # Nombres de host DNS en la VPC. Valores predeterminados falsos.
  enable_classiclink               = false # Solo válido en regiones y cuentas que admiten EC2 Classic
  enable_classiclink_dns_support   = false # Solo válido en regiones y cuentas que admiten EC2 Classic.
  assign_generated_ipv6_cidr_block = false #  puede especificar el rango de direcciones IP

  tags = {
    Name = "main" # Un mapa de etiquetas para asignar al recurso.
  }
}

output "vpc_id" {
  value       = aws_vpc.main.id # variables de salida las cuales las puede utilizar otro módulo
  description = "VPC id" # variables de salida las cuales las puede utilizar otro módulo
  sensitive   = false # variables de salida las cuales las puede utilizar otro módulo
}

```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform init # Inicializa un nuevo ambiente de trabajo
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto podemos ir a la consola de aws a validar que este creada la vpc 

ver imagenes


[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc.png?alt=media&token=b36ccfda-5a03-4558-ae69-db8e6c64028e
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc.png?alt=media&token=b36ccfda-5a03-4558-ae69-db8e6c64028e
)

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc%20select.png?alt=media&token=c0c60bf5-7f6e-4b43-a0ad-e6af24142712
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc%20select.png?alt=media&token=c0c60bf5-7f6e-4b43-a0ad-e6af24142712
)


[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc%20aws.png?alt=media&token=d6d582da-e1d6-495a-be64-fa80d008d74e
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/vpc%20aws.png?alt=media&token=d6d582da-e1d6-495a-be64-fa80d008d74e
)

En este punto nuestra arquitectura se visualiza así

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-vpc.png?alt=media&token=9e9d1464-dc6a-405f-94f4-d0608de877f9
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-vpc.png?alt=media&token=9e9d1464-dc6a-405f-94f4-d0608de877f9
)

A continuación creamos un archivo con nombre "internet-gateway.tf" e ingresamos la siguiente configuración

```json
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id # El ID de VPC para crear en

  tags = {
    Name = "main" # A map of tags to assign to the resourceSSSS
  }
}
```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto nuestra arquitectura se visualiza así

ver imagen

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-internet-gateway.png?alt=media&token=398156b4-0dc2-4039-8e72-9de53704fbd5
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-internet-gateway.png?alt=media&token=398156b4-0dc2-4039-8e72-9de53704fbd5
)

A continuación creamos un archivo con nombre "subnets.tf" e ingresamos la siguiente configuración

```json
    resource "aws_subnet" "public_1" {
  vpc_id                  = aws_vpc.main.id # atributo para garantizar el orden de las dependencias.
  cidr_block              = "192.168.0.0/18" # El bloque CIDR para la subred
  availability_zone       = "us-east-1a" # La AZ para la subred
  map_public_ip_on_launch = true # indicar que las instancias se lanzaron en la subred
  # Un mapa de etiquetas para asignar al recurso.
  tags = {
    Name                        = "public-us-east-1a"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb"    = 1
  }
}

resource "aws_subnet" "public_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "192.168.64.0/18"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name                        = "public-us-east-1b"
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb"    = 1
  }
}

resource "aws_subnet" "private_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "192.168.128.0/18"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name                              = "public-us-east-1a"
    "kubernetes.io/cluster/eks"       = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}

resource "aws_subnet" "private_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "192.168.192.0/18"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name                              = "public-us-east-1b"
    "kubernetes.io/cluster/eks"       = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}

```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto nuestra arquitectura se visualiza así

ver imagenes

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/subnet.png?alt=media&token=a8f2e786-4778-46eb-81ff-d0399aaba8f4
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/subnet.png?alt=media&token=a8f2e786-4778-46eb-81ff-d0399aaba8f4
)



[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-public-subnet-private-subnet.png?alt=media&token=3653e0e5-7cff-466f-afc3-091f5b199cea
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-public-subnet-private-subnet.png?alt=media&token=3653e0e5-7cff-466f-afc3-091f5b199cea
)

A continuación creamos un archivo con nombre "eips.tf" e ingresamos la siguiente configuración

```json
resource "aws_eip" "nat1" {
  depends_on = [
    aws_internet_gateway.main
  ]
}

resource "aws_eip" "nat2" {
  depends_on = [
    aws_internet_gateway.main
  ]
}
```

A continuación creamos un archivo con nombre "nat-gateways.tf" e ingresamos la siguiente configuración

```json
resource "aws_nat_gateway" "gw1" {
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.public_1.id

  tags = {
    Name = "NAT 1"
  }
}

resource "aws_nat_gateway" "gw2" {
  allocation_id = aws_eip.nat2.id
  subnet_id     = aws_subnet.public_2.id

  tags = {
    Name = "NAT 2"
  }
}
```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto nuestra arquitectura se visualiza así

ver imagenes

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/nat-gateway.png?alt=media&token=7c8cff82-d25f-4754-a311-6b2ef24acc81
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/nat-gateway.png?alt=media&token=7c8cff82-d25f-4754-a311-6b2ef24acc81
)


[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-nat-gateway.png?alt=media&token=84fc45cb-63aa-4c7a-ba35-8365693120b9
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-nat-gateway.png?alt=media&token=84fc45cb-63aa-4c7a-ba35-8365693120b9
)

A continuación creamos un archivo con nombre "roting-tables.tf" e ingresamos la siguiente configuración

```json
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "public"
  }
}

resource "aws_route_table" "private1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw1.id
  }

  tags = {
    Name = "private1"
  }
}

resource "aws_route_table" "private2" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw2.id
  }

  tags = {
    Name = "private2"
  }
}

```

A continuación creamos un archivo con nombre "route-table-association.tf" e ingresamos la siguiente configuración

```json
resource "aws_route_table_association" "public1" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public2" {
  subnet_id      = aws_subnet.public_2.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private1" {
  subnet_id      = aws_subnet.private_1.id
  route_table_id = aws_route_table.private1.id
}

resource "aws_route_table_association" "private2" {
  subnet_id      = aws_subnet.private_2.id
  route_table_id = aws_route_table.private2.id
}

```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto nuestra arquitectura se visualiza así

ver imagenes

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/route-table.png?alt=media&token=a175e8b5-3c9a-454f-adfc-daa5cd1cd17d
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/route-table.png?alt=media&token=a175e8b5-3c9a-454f-adfc-daa5cd1cd17d
)


[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-route-table.png?alt=media&token=c5566823-e00f-40f5-a8f8-69809fd24a5c
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-route-table.png?alt=media&token=c5566823-e00f-40f5-a8f8-69809fd24a5c
)


A continuación creamos un archivo con nombre "eks.tf" e ingresamos la siguiente configuración

```json
resource "aws_iam_role" "eks_cluster" {
  name = "eks-cluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Principal": {
              "Service": "eks.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
      }
    ]
}
  POLICY
}

resource "aws_iam_role_policy_attachment" "amazon_eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_eks_cluster" "eks" {
  name = "eks"

  role_arn = aws_iam_role.eks_cluster.arn
  version  = "1.18"
  vpc_config {
    endpoint_private_access = false
    endpoint_public_access  = true
    subnet_ids = [
      aws_subnet.public_1.id,
      aws_subnet.public_2.id,
      aws_subnet.private_1.id,
      aws_subnet.private_2.id
    ]
  }
  depends_on = [
    aws_iam_role_policy_attachment.amazon_eks_cluster_policy
  ]
}
```

A continuación creamos un archivo con nombre "eks-node-groups.tf" e ingresamos la siguiente configuración

```json
resource "aws_iam_role" "nodes_general" {
  name = "eks-node-group-general"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
  POLICY
}

resource "aws_iam_role_policy_attachment" "amazon_eks_worker_node_policy_general" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_iam_role_policy_attachment" "amazon_eks_cni_policy_general" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_iam_role_policy_attachment" "amazon_ec2_container_registry_read_only" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes_general.name
}

resource "aws_eks_node_group" "nodes_general" {
  cluster_name    = aws_eks_cluster.eks.name
  node_group_name = "nodes-general"
  node_role_arn   = aws_iam_role.nodes_general.arn
  subnet_ids = [
    aws_subnet.private_1.id,
    aws_subnet.private_2.id
  ]
  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }
  ami_type = "AL2_x86_64"

  capacity_type = "ON_DEMAND"

  disk_size = 20

  force_update_version = false
  instance_types       = ["t3.small"]

  labels = {
    role = "nodes-general"
  }

  version = "1.18"

  depends_on = [
    aws_iam_role_policy_attachment.amazon_eks_worker_node_policy_general,
    aws_iam_role_policy_attachment.amazon_eks_cni_policy_general,
    aws_iam_role_policy_attachment.amazon_ec2_container_registry_read_only
  ]
}
```

Ejecutamos los siguientes comandos
```sh
    terraform fmt # permite formatear el código
    terraform validate # valida que los archivos esten bien construidos
    terraform plan # muestra lo que desea crear
    terraform apply # crear la infrastructura si se manda el comando solo con apply debe dar "yes"       para aceptar el proceso de creación
```

En este punto nuestra arquitectura se visualiza así

ver imagenes

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-eks.png?alt=media&token=08ca570b-0e94-42df-9c62-5383e474d085
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-eks.png?alt=media&token=08ca570b-0e94-42df-9c62-5383e474d085
)

Ejecutamos los siguientes comandos
```sh
    aws eks --region us-east-1 update-kubeconfig --name eks --profile prueba  # permite actualizar la configuración
    kubectl get svc
```

A continuación creamos un archivo con nombre "app.yaml" e ingresamos la siguiente configuración

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: internal-nginx-service
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-type: nlb
    service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: 'true'
    service.beta.kubernetes.io/aws-load-balancer-internal: 0.0.0.0/0
spec:
  selector:
    app: nginx
  type: LoadBalancer
  ports:
    - protocol: TCP
      port: 80
---
apiVersion: v1
kind: Service
metadata:
  name: external-nginx-service
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-type: nlb
    service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: 'true'
spec:
  selector:
    app: nginx
  type: LoadBalancer
  ports:
    - protocol: TCP
      port: 80

```

Ejecutamos el siguiente comando
```sh
   kubectl apply -f app.yaml
   kubectl get pods
   kebectl get svc
```

En este punto ya debemos tener una ip external corriendo de type LoadBalancer con nombre external-nginx-service

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-ekss.png?alt=media&token=6204e219-0bd9-4417-a7eb-64a43f57c50b
)](https://firebasestorage.googleapis.com/v0/b/san-web-200c8.appspot.com/o/diagrama-ekss.png?alt=media&token=6204e219-0bd9-4417-a7eb-64a43f57c50b
)


### Referencias
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest
https://www.youtube.com/channel/UCeLvlbC754U6FyFQbKc0UnQ
