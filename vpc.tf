resource "aws_vpc" "main" {
  cidr_block                       = "192.168.0.0/16" # El bloque CIDR para la VPC
  instance_tenancy                 = "default"        # Una opción de arrendamiento para instancias lanzadas
  enable_dns_support               = true             # para habilitar / deshabilitar el soporte de DNS en el
  enable_dns_hostnames             = true             # Nombres de host DNS en la VPC. Valores predeterminados falsos.
  enable_classiclink               = false            # Solo válido en regiones y cuentas que admiten EC2 Classic
  enable_classiclink_dns_support   = false            # Solo válido en regiones y cuentas que admiten EC2 Classic.
  assign_generated_ipv6_cidr_block = false            #  puede especificar el rango de direcciones IP

  tags = {
    Name = "main" # Un mapa de etiquetas para asignar al recurso.
  }
}

output "vpc_id" {
  value       = aws_vpc.main.id # variables de salida las cuales las puede utilizar otro módulo
  description = "VPC id"        # variables de salida las cuales las puede utilizar otro módulo
  sensitive   = false           # variables de salida las cuales las puede utilizar otro módulo
}
