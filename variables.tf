variable "db_password" {
  default     = "postgress"
  description = "RDS root user password"
  sensitive   = true
}
