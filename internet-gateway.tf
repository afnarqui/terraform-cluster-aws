resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id # El ID de VPC para crear en

  # Un mapa de etiquetas para asignar al recurso.
  tags = {
    Name = "main" 
  }
}